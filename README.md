# Toys for Tots Donation Site Map
Toys for Tots mission is to provide toys, books, and other gifts to less fortunate children during the holidays.  This tool helps you locate toy donation drop off locations near you, or anywhere in the United States.



[![Searchable Map Template screenshot](https://bitbucket.org/johnkramlich/stltoys4tots/raw/master/images/example.png)](https://donateit.to/toys-for-tots/)

[Visit the site](https://donateit.to/toys-for-tots/)

## Features

* Displays over 20,000 toy donation sites for the 2018 holiday season
* Address search (with variable radius and geocomplete)
* Mobile and tablet friendly using responsive design
  
## Dependencies

* [Google Fusion Tables](http://www.google.com/fusiontables/Home)
* [Google Maps API V3](https://developers.google.com/maps/documentation/javascript)
* [jQuery](http://jquery.org)
* [jQuery Address](https://github.com/asual/jquery-address)
* [Bootstrap 3.2.0](http://getbootstrap.com/)

## Contributors

* [John Kramlich](https://johnkramlich.com) - customized for Toys for Tots
* [Derek Eder](http://derekeder.com/) - creator of the original template